<img align="right" src="https://git.rwth-aachen.de/IENT/ient.nb/raw/master/figures/rwth_ient_logo@2x.png" alt="Logo Institut für Nachrichtentechnik | RWTH Aachen University" width="240px">

# Signalübertragung Demos

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2FIENT%2Fsignaluebertragung-demos/master?urlpath=lab/tree/index.ipynb)

## Introduction

This repository contains demos for selected topics of the book ["Signalübertragung"](https://doi.org/10.1007/978-3-642-53901-5), by Jens-Rainer Ohm (chair of [Institut für Nachrichtentechnik](http://www.ient.rwth-aachen.de)) at [RWTH Aachen University](http://www.rwth-aachen.de) and Hans Dieter Lüke (former chair).

Visit the notebook [index.ipynb](index.ipynb) for a table of contents.

## Quick Start

Run the notebooks directly online [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2FIENT%2Fsignaluebertragung-demos/master?urlpath=lab/tree/index.ipynb). Please note the following limitations:

* The starting process of the session may take up to one minute.
* Please note that the session will be cancelled after 10 minutes of user inactivity.

## Usage

To be able to open the demos locally, you need to have a Python environment as well as JupyterLab installed. We recommend to use Anaconda for a local installation.

### Anaconda

To run the notebooks on your local machine, you may use [Anaconda](https://www.anaconda.com/) (using `pip` is also possible for experienced users. You have to install all the requirements listed in `binder/environment.yml` and install the two `jupyter labextension ...` commands listed in `binder/postBuild`).

#### Installation

* Install [Anaconda](https://www.anaconda.com/).
* Download this repository to your local disk. You can download via `git clone --recurse-submodules git@git.rwth-aachen.de:IENT/signaluebertragung-demos.git`.
* It is highly recommended to run the notebooks in an isolated Anaconda environment. You can create a new environment called `suelab` from the provided `binder/environment.yml` by running `conda env create -f binder/environment.yml` in the Anaconda prompt. This makes sure that all required packages are installed amd don't interfere with the packages in your base environment.
* Activate this environment with `conda activate suelab`.
* Run two final commands in the Anaconda prompt (with activated `suelab` environment):

  ```bash
  chmod +x binder/postBuild
  binder/postBuild
  ```

  If the latter command fails, please open `binder/postBuild` and execute the commands listed there manually.

#### Run

* Activate the environment  with `conda activate suelab`.
* Run JupyterLab  `jupyter lab`. In your browser, JupyterLab should start. You can then open `index.ipynb` for an overview over all notebooks.
* You can deactivate the environment with `conda deactivate` (and switch back to it with `conda activate suelab`).

### Docker

For advanced users only: If you happen to have Docker installed, you can start a local dockerized JupyterLab with all dependencies already taken care off (clone this repository and run the Docker image)

```bash
 git clone --recurse-submodules https://git.rwth-aachen.de/IENT/signaluebertragung-demos.git sue-demos
 docker run --name='sue-demos' --rm -it -p 8888:8888 -v ${pwd}/sue-demos:/home/jovyan/work registry.git.rwth-aachen.de/ient/gdet3-demos:master
```

Copy and paste the displayed link to your favorite browser.

## Contact

* If you found a bug, please use the [issue tracker](https://git.rwth-aachen.de/IENT/signaluebertragung-demos/issues).
* In all other cases, please contact [Christian Rohlfing](http://www.ient.rwth-aachen.de/cms/c_rohlfing/).

The code is licensed under the [MIT license](https://opensource.org/licenses/MIT).